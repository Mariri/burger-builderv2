import React from 'react'

import burgerLogo from '../../assets/images/burger-logo.png'
import './Logo.scss'

const logo = (props) => (
  <div className="Logo" style={props.height}>
    <img src={burgerLogo} alt="MyBurger" className="Logo__image" />
  </div>
)

export default logo