import React from 'react'

import Logo from '../../Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import Backdrop from '../../UI/Backdrop/Backdrop'

import './SideDrawer.scss'

const sideDrawer = (props) => {
  let attachedClasses = "SideDrawer SideDrawer__close";
  if(props.open) {
    attachedClasses = "SideDrawer SideDrawer__open";
  }
  return (
    <>
      <Backdrop
        show={props.open}
        clicked={props.closed} />
      <div className={attachedClasses} onClick={props.closed}>
        <div className="SideDrawer__logo">
          <Logo />
        </div>
        <nav>
          <NavigationItems isAuthenticated={props.isAuth} />
        </nav>
      </div>
    </>
  )
}

export default sideDrawer