import React from 'react'

import './Toolbar.scss'
import Logo from '../../Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle'

const toolbar = (props) => (
  <header className="Toolbar">
    <DrawerToggle clicked={props.open} />
    <div className="Toolbar__logo">
      <Logo />
    </div>
    <nav className="Toolbar__nav">
      <NavigationItems isAuthenticated={props.isAuth} />
    </nav>
  </header>
)

export default toolbar