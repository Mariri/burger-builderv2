import React from 'react'

import './Order.scss'

const order = (props) => {
  let orderIngredients = Object.keys(props.ingredients)
    .map(igKey => {
      return <span
        style={{
          textTransform: 'capitalize',
          display: 'inline-block',
          margin: '0 8px',
          border: '1px solid #ccc',
          padding: '5px'
        }}
        key={igKey}>{igKey} ({props.ingredients[igKey]})</span>
    })

  return (
    <div className="Order">
      <p>Ingredients: {orderIngredients}</p>
      <p>Price: <strong>PHP {Number.parseFloat(props.price).toFixed(2)}</strong></p>
    </div>
  )
}

export default order