import React from 'react'

import './Spinner.scss'

const spinner = () => (
  <div className="Spinner">Loading...</div>
)

export default spinner