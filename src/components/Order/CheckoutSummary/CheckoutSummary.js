import React from 'react'

import './CheckoutSummary.scss'
import Burger from '../../Burger/Burger'
import Button from '../../UI/Button/Button'

const checkoutSummary = (props) => {
  return (
    <div className="Checkout">
      <h1>We hope it tastes well!</h1>
      <div style={{width: '100%', margin: 'auto'}}>
        <Burger ingredients={props.ingredients} />
      </div>
      <Button
        buttonType="Button__danger"
        clicked={props.checkoutCancelled}>CANCEL</Button>
      <Button
        buttonType="Button__success"
        clicked={props.checkoutContinued}>CONTINUE</Button>
    </div>
  )
}

export default checkoutSummary