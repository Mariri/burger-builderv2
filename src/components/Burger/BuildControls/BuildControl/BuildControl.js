import React from 'react'

import './BuildControl.scss'

const buildControl = (props) => (
  <div className="BuildControl">
    <div className="BuildControl__label">{props.label}</div>
    <button className="BuildControl__button BuildControl__button--less" 
      onClick={props.removed}
      disabled={props.disabled}>Less</button>
    <button className="BuildControl__button BuildControl__button--more"
      onClick={props.added}>More</button>
  </div>
)

export default buildControl