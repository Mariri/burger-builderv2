import React from 'react'

import './Input.scss'

const input = (props) => {
  let inputElement = null
  const inputClasses = ['Input__element']

  if(props.invalid && props.shouldValidate && props.touched) {
    inputClasses.push('Input__invalid')
  }

  switch(props.elementType) {
    case('input'):
      inputElement = <input
        className={inputClasses.join(' ')}
        {...props.elementConfig}
        value={props.value}
        onChange={props.changed} />
      break
    case('textarea'):
      inputElement = <textarea
        className="Input__element"
        {...props.elementConfig}
        value={props.value}
        onChange={props.changed} />
      break
    case('select'):
      inputElement = (
        <select
          className={inputClasses.join(' ')}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}>
            {props.elementConfig.options.map(option => (
             <option key={option.value} value={option.value}>{option.displayValue}</option> 
            ))}
        </select>
      )
      break
    default:
      inputElement = <input className={inputClasses.join(' ')}
        {...props.elementConfig}
        value={props.value}
        onChange={props.changed} />
      break
  }

  return (
    <div className="Input">
      <label className="Input__label">{props.label}</label>
      {inputElement}
    </div>
  )
}

export default input