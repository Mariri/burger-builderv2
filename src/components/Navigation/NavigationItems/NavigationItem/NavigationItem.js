import React from 'react'
import { NavLink } from 'react-router-dom'

import './NavigationItem.scss'

const navigationItem = (props) => {
  return <li className="NavigationItem">
    <NavLink
      exact={props.exact}
      className="NavigationItem__link"
      activeClassName="NavigationItem__link--active"
      to={props.link}>{props.children}</NavLink>
  </li>
}

export default navigationItem